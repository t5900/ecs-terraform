variable "app_name" {
  type        = string
  default     = "django-twitter"
  description = "Name of the container application"
}

variable "app_port" {
  description = "Application port"
  default     = 8000
  type        = number
}

variable "region" {
  type        = string
  default     = "us-east-1"
  description = "The AWS region where resources have been deployed"
}

variable "availability_zones" {
  type        = list(string)
  default     = ["us-east-1a", "us-east-1b", "us-east-1c"]
  description = "List of availability zones for the selected region"
}

variable "environment" {
  description = "Applicaiton environment"
  default     = "dev"
  type        = string
}

variable "app_count" {
  default = 1
  type    = number
}

variable "workers_count" {
  default = 10
  type    = number
}

variable "docker_image" {
  default = "249736386209.dkr.ecr.us-east-1.amazonaws.com/twitter-new:latest"
}