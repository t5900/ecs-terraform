terraform {
  backend "s3" {
    bucket = "ecs-bryi"
    key    = "terraform.tfstate"
    region = "us-east-1"
  }
}

module "vpc" {
  source                        = "./vpc"
  app_name                      = var.app_name
  app_port                      = var.app_port
  number_of_private_subnets     = 2
  number_of_db_subnets          = 2
  environment                   = var.environment
  security_group_ecs_tasks_name = var.app_name
  vpc_tag_name                  = "${var.app_name}-vpc"
  private_subnet_tag_name       = "${var.app_name}-pvt-subnets"
  db_subnet_tag_name            = "${var.app_name}-db-subnets"
  availability_zones            = ["us-east-1a", "us-east-1b", "us-east-1c"]
  region                        = var.region
}

resource "random_string" "rds_password" {
  length           = 12
  special          = true
  override_special = "!#$&"

  keepers = {
    kepeer1 = var.app_name
    //keperr2 = var.something
  }
}

// Store Password in SSM Parameter Store
resource "aws_ssm_parameter" "rds_password" {
  name        = "/prod/postgresql"
  description = "Master Password for RDS"
  type        = "SecureString"
  value       = random_string.rds_password.result
  depends_on = [
    module.vpc
  ]
}

// Get Password from SSM Parameter Store
data "aws_ssm_parameter" "my_rds_password" {
  name       = "/prod/postgresql"
  depends_on = [aws_ssm_parameter.rds_password]
}

module "rds" {
  source          = "./rds"
  subnet_ids      = module.vpc.db_subnet_ids
  db_identifier   = var.app_name
  engine_type     = "postgres"
  engine_version  = "11.6"
  db_name         = "django"
  username        = "django"
  db_password     = data.aws_ssm_parameter.my_rds_password.value
  security_groups = tolist([module.vpc.rds_security_group_id])
  depends_on = [
    module.vpc
  ]
}

module "cache" {
  source                 = "./cache"
  cluster_id             = "${var.app_name}-cluster"
  subnet_groups          = module.vpc.private_subnet_ids
  cache_node_number      = 1
  security_group_ids_var = tolist([module.vpc.redis_security_group_id])
  depends_on = [
    module.vpc
  ]
}

module "ecs" {
  source               = "./ecs"
  app_name             = var.app_name
  app_name_worker      = "${var.app_name}-worker"
  app_port             = var.app_port
  app_count            = var.app_count
  workers_count        = var.workers_count
  db_user              = module.rds.db_user
  db_password          = data.aws_ssm_parameter.my_rds_password.value
  db_url               = module.rds.db_endpoint
  redis_url            = "redis://${module.cache.cache_endpoint}"
  app_image            = var.docker_image
  fargate_cpu          = 512
  fargate_memory       = 1024
  nlb_target_group_arn = module.vpc.nlb_target_group_arn
  name                 = var.app_name
  cluster_tag_name     = var.app_name
  vpc_id               = module.vpc.vpc_id
  environment          = var.environment
  aws_security_group_ecs_tasks_id = tolist([
    module.vpc.ecs_tasks_security_group_id,
    module.vpc.rds_security_group_id,
    module.vpc.redis_security_group_id
  ])
  private_subnet_ids = module.vpc.private_subnet_ids
  depends_on = [
    module.vpc,
    module.rds,
    module.cache
  ]
}

module "api_gateway" {
  source                 = "./api_gateway"
  name                   = var.app_name
  integration_input_type = "HTTP_PROXY"
  path_part              = "{proxy+}"
  app_port               = var.app_port
  nlb_dns_name           = module.vpc.nlb_dns
  nlb_arn                = module.vpc.nlb_arn
  environment            = var.environment
  depends_on = [
    module.ecs
  ]
}

module "dns" {
  source            = "./dns_zone"
  domain_name       = "bryidomain.tk"
  alias_domain_name = "api.bryidomain.tk"
  api_dns_name      = module.api_gateway.invoke_url
  api_zone_id       = var.region
  depends_on = [
    module.api_gateway
  ]
}