output "cache_endpoint" {
    value = "${join(":", tolist([aws_elasticache_cluster.cache.cache_nodes.0.address, aws_elasticache_cluster.cache.cache_nodes.0.port]))}"
}