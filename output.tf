output "db_endpoint" {
  value = module.rds.db_endpoint
}

output "db_password" {
  value     = data.aws_ssm_parameter.my_rds_password.value
  sensitive = true
}

output "db_user" {
  value = module.rds.db_user
}

output "cache_endpoint" {
  value = module.cache.cache_endpoint
}

output "api_url" {
  value = module.api_gateway.invoke_url
}