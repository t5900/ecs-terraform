resource "aws_route53_zone" "primary_zone" {
  name = var.domain_name
}

resource "aws_api_gateway_domain_name" "example" {
  certificate_arn = "arn:aws:acm:us-east-1:249736386209:certificate/c702a64c-6ab4-4691-811e-6020afee12a5"
  domain_name     = var.alias_domain_name
}

resource "aws_route53_record" "apigateway_alias" {
  zone_id = aws_route53_zone.primary_zone.zone_id
  name    = var.alias_domain_name
  type    = "A"

  alias {
    name                   = aws_api_gateway_domain_name.example.cloudfront_domain_name
    zone_id                = aws_api_gateway_domain_name.example.cloudfront_zone_id
    evaluate_target_health = true
  }
}