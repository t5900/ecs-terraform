variable "domain_name" {
  default = "bryidomain.tk"
}

variable "alias_domain_name" {
  default = "alb.bryidomain.tk"
}

variable "api_dns_name" {
  default = "some_dns_name"
}

variable "api_zone_id" {
  default = "us-east-1"
}