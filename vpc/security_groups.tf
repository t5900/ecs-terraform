### Security Group Setup

# Traffic to the ECS Cluster should only come from the NLB
# or AWS services through an AWS PrivateLink
resource "aws_security_group" "ecs_tasks" {
  name        = "${var.security_group_ecs_tasks_name}-${var.environment}"
  description = var.security_group_ecs_tasks_description
  vpc_id      = "${aws_vpc.custom_vpc.id}"

  ingress {
    protocol    = "tcp"
    from_port   = var.app_port
    to_port     = var.app_port
    cidr_blocks = [var.vpc_cidr_block]
  }

  ingress {
    protocol        = "tcp"
    from_port       = 443
    to_port         = 443
    cidr_blocks = [var.vpc_cidr_block]
  }

  egress {
    from_port       = 443
    to_port         = 443
    protocol        = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "rds-sg" {
  name        = "rds-sg-${var.environment}"
  description = "allow inbound access from the ECS only"
  vpc_id      = "${aws_vpc.custom_vpc.id}"

  ingress {
    protocol        = "-1"
    from_port       = 0
    to_port         = 0
    self            = true
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "redis-sg" {
  name        = "redis-sg-${var.environment}"
  description = "allow inbound access from the ECS only"
  vpc_id      = "${aws_vpc.custom_vpc.id}"

  ingress {
    protocol        = "-1"
    from_port       = 0
    to_port         = 0
    self = true
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}