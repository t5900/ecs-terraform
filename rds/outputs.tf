output "db_endpoint" {
  value = aws_db_instance.default.address
}

output "db_endpoint_port" {
  value = aws_db_instance.default.port
}

output "db_user" {
  value = aws_db_instance.default.username
}