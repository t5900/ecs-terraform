resource "aws_ecs_task_definition" "main" {
  family                   = var.app_name
  task_role_arn            = aws_iam_role.task_role.arn
  execution_role_arn       = aws_iam_role.main_ecs_tasks.arn
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.fargate_cpu
  memory                   = var.fargate_memory
  container_definitions = jsonencode([
    {
      name : var.app_name,
      image : var.app_image,
      cpu : var.fargate_cpu,
      memory : var.fargate_memory,
      networkMode : "awsvpc",
      entrypoint: ["./entrypoint.sh"],
      portMappings : [
        {
          containerPort : var.app_port
          protocol : "tcp",
          hostPort : var.app_port
        }
      ],
      logConfiguration: {
        logDriver: "awslogs",
        options: {
            awslogs-group: "awslogs-${var.app_name}-api",
            awslogs-region: "us-east-1",
            awslogs-stream-prefix: "awslogs-example",
            awslogs-create-group: "true"
        }
      },
      environment : [
          {
            name: "SECRET_KEY",
            value: "django-insecure-7-d54v=vy+d)!5p19p7mzq%w)8u&#53g1+!f!)nb^!25pw3o#3"
          },
          {
            name: "DEBUG",
            value: "0"
          },
          {
            name: "DB_NAME",
            value: "django"
          },
          {
            name: "DB_USER",
            value: var.db_user
          },
          {
            name: "DB_PASSWORD",
            value: var.db_password
          },
          {
            name: "DB_HOST",
            value: var.db_url
          },
          {
            name: "DB_PORT",
            value: "5432"
          },
          {
            name: "REDIS",
            value: var.redis_url
          },
      ],
    }
  ])
}

resource "aws_ecs_task_definition" "migrations" {
  depends_on = [
    aws_iam_role.task_role,
    aws_iam_role.main_ecs_tasks
  ]
  family                   = "${var.app_name}-migrations"
  task_role_arn            = aws_iam_role.task_role.arn
  execution_role_arn       = aws_iam_role.main_ecs_tasks.arn
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.fargate_cpu
  memory                   = var.fargate_memory
  container_definitions = jsonencode([
    {
      name : var.app_name,
      image : var.app_image,
      cpu : var.fargate_cpu,
      memory : var.fargate_memory,
      networkMode : "awsvpc",
      entrypoint: ["./migrations.sh"],
      portMappings : [
        {
          containerPort : var.app_port
          protocol : "tcp",
          hostPort : var.app_port
        }
      ],
      logConfiguration: {
        logDriver: "awslogs",
        options: {
            awslogs-group: "awslogs-${var.app_name}-migrations",
            awslogs-region: "us-east-1",
            awslogs-stream-prefix: "awslogs-example",
            awslogs-create-group: "true"
        }
      },
      environment : [
          {
            name: "SECRET_KEY",
            value: "django-insecure-7-d54v=vy+d)!5p19p7mzq%w)8u&#53g1+!f!)nb^!25pw3o#3"
          },
          {
            name: "DEBUG",
            value: "0"
          },
          {
            name: "DB_NAME",
            value: "django"
          },
          {
            name: "DB_USER",
            value: var.db_user
          },
          {
            name: "DB_PASSWORD",
            value: var.db_password
          },
          {
            name: "DB_HOST",
            value: var.db_url
          },
          {
            name: "DB_PORT",
            value: "5432"
          },
          {
            name: "REDIS",
            value: var.redis_url
          },
      ],
    }
  ])
}

resource "aws_ecs_task_definition" "workers" {
  family                   = "${var.app_name}-workers"
  task_role_arn            = aws_iam_role.task_role.arn
  execution_role_arn       = aws_iam_role.main_ecs_tasks.arn
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.fargate_cpu
  memory                   = var.fargate_memory
  container_definitions = jsonencode([
    {
      name : "${var.app_name}-worker",
      image : var.app_image,
      cpu : var.fargate_cpu,
      memory : var.fargate_memory,
      networkMode : "awsvpc",
      command: ["celery", "--app", "twitter", "worker", "-l", "INFO"],
      logConfiguration: {
        logDriver: "awslogs",
        options: {
            awslogs-group: "awslogs-${var.app_name}-workers",
            awslogs-region: "us-east-1",
            awslogs-stream-prefix: "awslogs-example",
            awslogs-create-group: "true"
        }
      },
      environment : [
          {
            name: "SECRET_KEY",
            value: "django-insecure-7-d54v=vy+d)!5p19p7mzq%w)8u&#53g1+!f!)nb^!25pw3o#3"
          },
          {
            name: "DEBUG",
            value: "0"
          },
          {
            name: "DB_NAME",
            value: "django"
          },
          {
            name: "DB_USER",
            value: var.db_user
          },
          {
            name: "DB_PASSWORD",
            value: var.db_password
          },
          {
            name: "DB_HOST",
            value: var.db_url
          },
          {
            name: "DB_PORT",
            value: "5432"
          },
          {
            name: "REDIS",
            value: var.redis_url
          },
      ],
    }
  ])
}
